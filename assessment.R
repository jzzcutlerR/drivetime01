# ============================================================================ #
#' For each numbered section below, write R code and comments to solve the 
#' problem or to show your rationale.
#' 
#' For sections that ask you to give outputs, you can paste the output directly 
#' below the code that runs it or you can provide outputs in separate files.
#' 
#' Please return your modified R script along with any supporting files.
# ============================================================================ #


# Step 1 -----------------------------------
# ============================================================================ #
#' 1. Load in the dataset from the accompanying file "account-defaults.csv".
#'    
#'    This dataset contains information about loan accounts that either went
#'    delinquent or stayed current on payments within the loan's first year.
#'    FirstYearDelinquency is the outcome variable, all others are predictors.
#'    The objective of modeling with this dataset is to be able to predict
#'    the probability that new accounts will become delinquent; it is primarily
#'    valuable to understand lower-risk accounts versus higher-risk accounts
#'    (and not just to predict 'yes' or 'no' for new accounts).
#'    
#'    FirstYearDelinquency - indicates whether the loan went delinquent within
#'    the first year of the loan's life (values of 1)
#'    
#'    AgeOldestIdentityRecord - number of months since the first record was
#'    reported by a national credit source
#'    
#'    AgeOldestAccount - number of months since the oldest account was opened
#'    
#'    AgeNewestAutoAccount - number of months since the most recent auto loan 
#'    or lease account was opened
#'    
#'    TotalInquiries - total number of credit inquiries on record
#'    
#'    AvgAgeAutoAccounts - average number of months since auto loan or lease 
#'    accounts were opened
#'    
#'    TotalAutoAccountsNeverDelinquent - total number of auto loan or lease
#'    accounts that were never delinquent
#'    
#'    WorstDelinquency - worst status of days-delinquent on an account in the
#'    first 12 months of an account's life; values of '365' indicate '365 or 
#'    greater'
#'    
#'    HasInquiryTelecomm - indicates whether one or more telecommunications
#'    credit inquires are on record within the last 12 months (values of 1)
#'    
# ============================================================================ #


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
#You HAVE to source() DriveTime01.R before you do anything else.
#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
#General note: I'm going to put intermediate values in junk variables: a, b, x,
#y, and z.  This will allow me to make comments on intermediate values more
#easily.  For instance, it is much harder to comment this:
#   fn1(fn2(foo, fn3(bar, bax, baz)))
#than it is to comment this:
#   a <- fn3(bar, bax, baz)
#   b <- fn2(foo, a)
#   fn1(b)
#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# To see the output of this and all other steps, 
# 1) Open assessment.Rmd
# 2) Press Ctrl+Alt+R to run all sections
# 3) Press Ctrl+Shift+K to see the output as a web page.  You can use Ctrl-+ to 
#    zoom in and Ctrl+- to zoom out.
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


# Step 2 -----------------------------------
# ============================================================================ #
#' 2. Perform an exploratory data analysis on the accounts data, including
#'    summary statistics and visualizations of the distributions and 
#'    relationships.
# ============================================================================ #


#For output run assessment.Rmd under RStudio and then press Ctrl+Shift+K or see 

#Get all sorts of summary statistics for the data.table and attach names to each
#value.  We store the values in a list because if we use a vector everything
#will be converted to 'character' because the column names are character.
#Besides, we need a list for rbindlist() anyway.
Step2.Statistics <- function(nCol) {
  x <- dtAccountDefaults[, get(nCol)] #get the actual data for the named column
  
  #Create an empty list and vector so all the other lines are the same and can
  #be moved around with impugnity
  values <- list()
  sNames <- c('')[0]
  
  #The first column of the output will be the name of the column
  values <- c(values, nCol)
  sNames <- c(sNames, 'Column')
  names(values) <- sNames
  
  values <- c(values, typeof(x))
  sNames <- c(sNames, 'typeof')
  names(values) <- sNames
  
  values <- c(values, length(unique(x)))
  sNames <- c(sNames, 'UniqValues')
  names(values) <- sNames
  
  values <- c(values, fivenum(x))
  sNames <- c(sNames,
    'minimum', 'lower-hinge', 'median', 'upper-hinge', 'maximum'
  )
  names(values) <- sNames
  
  values <- c(values, mean(x, na.rm = T))
  sNames <- c(sNames, 'mean')
  names(values) <- sNames
  
  values <- c(values, sd(x, na.rm = T))
  sNames <- c(sNames, 'sd')
  names(values) <- sNames
  
  values <- c(values, var(x, na.rm = T))
  sNames <- c(sNames, 'var')
  names(values) <- sNames
  
  values <- c(values, values[['maximum']] - values[['minimum']])
  sNames <- c(sNames, 'width')
  names(values) <- sNames
  
  #IQR can sometimes fail so we wrap it in tryCatch and return NA on an error.
  a <- tryCatch(IQR(x), error = function(x) NA)
  values <- c(values, a)
  sNames <- c(sNames, 'IQR')
  names(values) <- sNames
  
  values <- c(values, sum(is.na(x)))
  sNames <- c(sNames, 'NaCount')
  names(values) <- sNames
  
  values <- c(values, sum(x < 0, na.rm = T))
  sNames <- c(sNames, 'NegCount')
  names(values) <- sNames
  
  values <- c(values, sum(x == 0, na.rm = T))
  sNames <- c(sNames, 'ZeroCount')
  names(values) <- sNames
  
  values <- c(values, sum(x > 0, na.rm = T))
  sNames <- c(sNames, 'PosCount')
  names(values) <- sNames
  
  #The last column of the output will also be the name of the column
  values <- c(values, nCol)
  sNames <- c(sNames, 'Column2')
  names(values) <- sNames
  
  return(values)
}


Step2.Relationships <- function(nCol) {
  plot_scatterplot(
    dtAccountDefaults, 
    by = nCol, 
    title = paste('y Axis is', nCol)
  )
}


#We're going to do some data prep.  This is all stuff we can do before the
#train/test split because, for each row, we're not using any data from any other
#row.
Step2.DataPrep01 <- function(dtAccountDefaults) {

  #If they don't have any accounts then set the age of the newest to 0
  dtAccountDefaults[is.na(AgeNewestAutoAccount), AgeNewestAutoAccount := 0]
  
  #If they don't have any accounts then set the age of the oldest to 0
  dtAccountDefaults[is.na(AgeOldestAccount), AgeOldestAccount := 0]
  
  #If they don't have any accounts then set the average age to 0
  dtAccountDefaults[is.na(AvgAgeAutoAccounts), AvgAgeAutoAccounts := 0]
  
  #If they don't have any accounts then set the number of delinquent accounts to 0
  dtAccountDefaults[is.na(TotalAutoAccountsNeverDelinquent), TotalAutoAccountsNeverDelinquent := 0]
  
  #If they haven't had any inquiries then set the count to 0
  dtAccountDefaults[is.na(TotalInquiries), TotalInquiries := 0]
  
  #If they haven't had any delinquencies then set the days to 0
  dtAccountDefaults[is.na(WorstDelinquency), WorstDelinquency := 0]
  
  #The doc says WorstDelinquency should not be more than 365 but it has almost
  #1,000 values that are 999.  We don't want to throw out that many rows so
  #we're going to assume it means a really big number instead of meaning NA.
  dtAccountDefaults[WorstDelinquency > 365, WorstDelinquency := 365]
  
  #In a departure from the above logic we're going to throw out rows with NA or
  #a negative number for AgeOldestIdentityRecord because we don't want to offer
  #credit to someone we can't identify.  Also, there are few of them so we're
  #not throwing out a lot of rows.
  a <- 'AgeOldestIdentityRecord'
  dtAccountDefaults <- dtAccountDefaults[!is.na(get(a)) & (get(a) >= 0), ]
  
  #We also throw out rows with NA or a negative number for HasInquiryTelecomm
  #because those values don't make sense for a boolean.  Also, there are few of
  #them so we're not throwing out a lot of rows.
  a <- 'HasInquiryTelecomm'
  dtAccountDefaults <- dtAccountDefaults[!is.na(get(a)) & (get(a) >= 0), ]
  
  return(dtAccountDefaults)
}


# Step 3 -----------------------------------
# ============================================================================ #
#' 3. Build one or more predictive model(s) on the accounts data using
#'    regression techniques.
#'    
#'    Identify the strongest predictor variables and provide interpretations.
#'    
#'    Identify and explain issues with the model(s) such as collinearity, etc.
#'    
#'    Calculate predictions and show model performance on out-of-sample data.
#'    
#'    Summarize out-of-sample data in tiers from highest-risk to lowest-risk.
# ============================================================================ #
FeatureImportanceGlm <- function(mGlm) {
  #Get the variable importances from caret::varImp
  a <- varImp(mGlm, scale = T)
  dtVarImpGlm <- data.table(Feature = rownames(a), Importance = a)
  setnames(dtVarImpGlm, 'Importance.Overall', 'Importance')
  x <- dtVarImpGlm[, sum(Importance)]
  dtVarImpGlm[, Importance := Importance / x]
  
  #Get the coefficients from the model and add them to dtVarImp.  we use merge()
  #because we have no idea what order glm() might put the cofficients in.  It
  #also allows us to easily get rid of the (Intercept) coefficient and any other
  #extraneous ones.
  a <- mGlm$coefficients
  dt <- data.table(Feature = names(a), Coefficient = a)
  dtVarImpGlm <- merge(dtVarImpGlm, dt, on = 'Feature', all.x = T)

  invisible(dtVarImpGlm)
}


FeatureImportanceGlmPlot <- function(dtVarImpGlm) {
  #Sore the variable importances by importance and plot them  
  setorder(dtVarImpGlm, Importance)
  par(mai=c(1,3,1,1))
  #barplot(x[, Importance], names.arg = x[, Feature], horiz = T)
  a <- barplot(
    dtVarImpGlm[, Importance], 
    cex.names = 0.75,
    horiz=TRUE,
    main="Logistic Regression Feature Importance", 
    names.arg=dtVarImpGlm[, Feature],
    las=1
  )
  a <- print(a)
  
  invisible(dtVarImpGlm)
}


Step3.Correlations <- function(dtIn, method) {
  #Get the correlations and convert them to a data.table
  mCor <- cor(dtIn, use = 'pairwise.complete.obs', method = method)
  dtCor <- as.data.table(mCor)
  
  #The conversion causes us to lose the row names
  nColAgainst <- 'CorAgainst'
  dtCor[, (nColAgainst) := rownames(mCor)]
  
  #Put the new CorAgainst column at the front of the data.table and put the rest
  #in alphabetical order
  a <- c(nColAgainst, sort(setdiff(names(dtCor), nColAgainst)))
  setcolorder(dtCor, a)
  
  #Put the names in the last column as well
  nColAgainst2 <- 'CorAgainst2'
  dtCor[, (nColAgainst2) := rownames(mCor)]
  
  #Put the rows  in alphabetical order to match the columns
  setorderv(dtCor, nColAgainst)
  
  invisible(dtCor)
}


Step3.AssignToGroups <- function(dtIn, nCol, iGroups) {
  #We use a temporary column to remember the original order of the rows.
  dtIn[, .order := 1:nrow(dtIn)]
  
  #Sort the rows by the column that determines group membership.  We sort
  #asscending because we have revered the meaning of FirstYearDelinquency so a
  #high score means a low probability of default.
  setorderv(dtIn, nCol)
  
  #Assign all the rows to equal sized groups
  dtIn[, Group := 1 + floor((iGroups * (1:nrow(dtIn)) - 1) / nrow(dtIn))]
  
  #Restore the original order and remove the temp column
  setorder(dtIn, .order)
  dtIn[, .order := NULL]
  return(dtIn)
}


# Step 4 -----------------------------------
# ============================================================================ #
#' 4. Split up the dataset by the WorstDelinquency variable. For each subset, 
#'    run a simple regression of FirstYearDelinquency ~ TotalInquiries. Extract 
#'    the predictor's coefficient and p-value from each model. Store the results
#'    in a list where the names of the list correspond to the values of
#'    WorstDelinquency.
# ============================================================================ #


Step4.LinearRegression <- function(dtIn, iWorstDelinquency) {
  aa <- 1
  a <- c('FirstYearDelinquency', 'TotalInquiries')
  dt <- dtIn[WorstDelinquency == iWorstDelinquency, .SD, .SDcols = a]
  mlm <- lm(FirstYearDelinquency ~ TotalInquiries, data = dt)
  mlmSmry <- summary(mlm)
  
  #The model might not be able to predict on TotalInquiries so we have to handle
  #that
  a <- mlmSmry$coefficients
  if ('TotalInquiries' %in% rownames(a)) {
    return(list(
      coefficient = a['TotalInquiries', 'Estimate'],
      pValue = a['TotalInquiries', 'Pr(>|t|)']
    ))
  }
  else {
    return(list(
      coefficient = NA,
      pValue = NA
    ))
  }
  
  aa <- 1
  aa <- 1
  aa <- 1
}


# Step 5 -----------------------------------
# ============================================================================ #
#' 5. Load in the dataset from the accompanying file "vehicle-depreciation.csv".
#'    
#'    The dataset contains information about vehicles that our company purchases
#'    at auction, sells to customers, repossess from defaulted accounts, and 
#'    finally re-sell at auction to recover some of our losses.
#'    
#'    Perform an analysis and/or build a predictive model that provides a method
#'    to estimate the depreciation of vehicle worth (from auction purchase to
#'    auction sale). Use whatever techniques you want to provide insight into
#'    the dataset and walk us through your results - this is your chance to show 
#'    off your analytical and storytelling skills!
#'    
#'    CustomerGrade - the credit risk grade of the customer
#'    
#'    AuctionPurchaseDate - the date that the vehicle was purchased at auction
#'    
#'    AuctionPurchaseAmount - the dollar amount spent purchasing the vehicle at
#'    auction
#'    
#'    AuctionSaleDate - the date that the vehicle was sold at auction
#'    
#'    AuctionSaleAmount - the dollar amount received for selling the vehicle at
#'    auction
#'    
#'    VehicleType - the high-level class of the vehicle
#'    
#'    Year - the year of the vehicle
#'    
#'    Make - the make of the vehicle
#'    
#'    Model - the model of the vehicle
#'    
#'    Trim - the trim of the vehicle
#'    
#'    BodyType - the body style of the vehicle
#'    
#'    AuctionPurchaseOdometer - the odometer value of the vehicle at the time
#'    of purchase at the auction
#'    
#'    AutomaticTransmission - indicates (with value of 1) whether the vehicle
#'    has an automatic transmission
#'    
#'    DriveType - the drivetrain type of the vehicle
# ============================================================================ #


Step5.ModelXgb <- function(xgb.args, xgb.params, xdmTrain, xdmWatch, xdmVal, xdmTest) {
  mc <- match.call()

  a <- xgb.args$feval
  if (!is.null(a) && (is.character(a) || is.function(a)))  {
    if (is.character(a)) {
      xgb.args$feval <- get(xgb.args$feval, envir = .GlobalEnv)
    }
    
    #Set the maximize parm if the custom function has the attribute
    a <- attr(xgb.args$feval, 'bMaximize')
    if (!is.null(a) && is.logical(a)) {
      xgb.args$maximize <- a
    }
  }
  else {
    xgb.params$eval_metric <- xgb.args$eval_metric
    xgb.args$feval <- NULL
  }
  
  aa <- 1
  mXgb1 <<- xgb.train(
    params = xgb.params, 
    data = xdmTrain, 
    nrounds = xgb.args$nrounds, 
    watchlist = list(train=xdmTrain, watch=xdmWatch), 
    print_every_n = xgb.args$print_every_n, 
    early_stopping_rounds = xgb.args$early_stopping_rounds, 
    maximize = xgb.args$maximize, 
    feval = xgb.args$feval #???dk
  )
  
  return(mXgb1)

  #model prediction
  predXgbVal <<- predict(mXgb1, xdmVal)
  predXgbTest <<- predict(mXgb1, xdmTest)
  
  #Get the y values
  yTest <- getinfo(xdmTest, 'label')
  yVal <- getinfo(xdmVal, 'label')
  
  #Get the percentage of TP at 30% of predictions
  aa <- 1
  a <- order(predXgbTest, decreasing = T)
  dXgbTpAt30Test <- sum(yTest[a[1:ceiling(.3 * length(a))]])/sum(yTest)
  a <- order(predXgbVal, decreasing = T)
  dXgbTpAt30Val <- sum(yVal[a[1:ceiling(.3 * length(a))]])/sum(yVal)
  
  #Get the AUC/ROC and plot  
  rocr.predTest <- prediction(predXgbTest, yTest)
  rocr.predVal <- prediction(predXgbVal, yVal)
  
  aa <- 1
  rocr.perfTest = performance(rocr.predTest, measure = "tpr", x.measure = "fpr")
  rocr.perfVal = performance(rocr.predVal, measure = "tpr", x.measure = "fpr")
  
  plot(rocr.perfTest, add = F, col = 'blue')
  plot(rocr.perfVal, add = T, col = 'red')
  abline(a=0, b= 1)  
  abline(h = 0.7)
  abline(v = 0.3)
  
  title(
    main = paste('Xgboost AUC on Grid Segments', nav(mc)), 
    sub = paste(nav(dXgbTpAt30Test), nav(dXgbTpAt30Val))
  )
  legend(
    "topleft", 
    inset=.05, 
    cex = 1, 
    title="Legend", 
    c("Top","Rest"), 
    horiz=TRUE, 
    lty=c(1,1), 
    lwd=c(2,2), 
    col=c("blue", "red"), 
    bg="grey96"
  )
  
  #roc1 <- plot(roc(x, a), print.auc = TRUE, col = "red")
  #title(
  #  main = paste('Xgboost AUC', nav(mc)), 
  #  sub = paste(navb(dim(xdmTrain)), navb(dim(xdmWatch)), navb(dim(xdmTest)))
  #)
  #abline(h = 0.7)
  #abline(v = 0.7)
}



#We don't care how far the predictions are from the actuals.  All we care about
#is minimizing the depreciation.  We do that by using a weighted average of the
#actual depreciations when they are sorted in order of increasing predition.
#The weighting pushes large actual values towards the end of the list.
Step5.XgboostFeval01 <- function(yPred, xdm, eps=1e-15, iRound = 6) {
  aa <- 1
  yActual <- getinfo(xdm, 'label')
  
  #Get the percentage of TP at 30% of predictions
  a <- order(yPred, decreasing = F)
  dSum <- sum(yActual[a] * ((length(yActual):1) / length(yActual)))

  return(list(
    metric = 'Step5.XgboostFeval01', 
    value = round(dSum, iRound)
  ))
}
attr(Step5.XgboostFeval01, 'bMaximize') <- F
